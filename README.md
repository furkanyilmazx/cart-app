# CART App

Cart App contains high tested class for use e-commerce applications

#### Test coverage

| File                       | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s |
| -------------------------- | ------- | -------- | ------- | ------- | ----------------- |
| All files                  | 98.76   | 93.94    | 100     | 98.73   |                   |
| src                        | 100     | 100      | 100     | 100     |                   |
| helper.js                  | 100     | 100      | 100     | 100     |                   |
| src/Campaign               | 96.15   | 91.67    | 100     | 96.15   |                   |
| Campaign.js                | 96      | 91.67    | 100     | 96      | 48                |
| CampaignTypes.js           | 100     | 100      | 100     | 100     |                   |
| index.js                   | 0       | 0        | 0       | 0       |                   |
| src/Cart                   | 100     | 92.86    | 100     | 100     |                   |
| Cart.js                    | 100     | 92.86    | 100     | 100     | 69                |
| index.js                   | 0       | 0        | 0       | 0       |                   |
| src/Category               | 100     | 100      | 100     | 100     |                   |
| Category.js                | 100     | 100      | 100     | 100     |                   |
| index.js                   | 0       | 0        | 0       | 0       |                   |
| src/Coupon                 | 95.65   | 81.82    | 100     | 95.65   |                   |
| Coupon.js                  | 95.45   | 81.82    | 100     | 95.45   | 48                |
| CouponTypes.js             | 100     | 100      | 100     | 100     |                   |
| index.js                   | 0       | 0        | 0       | 0       |                   |
| src/DeliveryCostCalculator | 100     | 100      | 100     | 100     |                   |
| DeliveryCostCalculator.js  | 100     | 100      | 100     | 100     |                   |
| index.js                   | 0       | 0        | 0       | 0       |                   |
| src/Product                | 100     | 100      | 100     | 100     |                   |
| Product.js                 | 100     | 100      | 100     | 100     |                   |
| index.js                   | 0       | 0        | 0       | 0       |                   |

## Dependencies

- node.js _`(12.13.0+)`_
- npm _`(6.12.0+)`_ or yarn _`(1.17.3+)`_

## Getting Started

### Usage

Clone repository with one of the following ways and start coding.

with HTTPS:

```shell
git clone https://github.com/furkanyilmazx/cartapp.git
```

with SSH:

```shell
git clone git@github.com:furkanyilmazx/cartapp.git
```

### Install

> Please use first this command to start app properly

```shell
yarn
```

### Development

```shell
yarn start
```

### Build

```shell
yarn run build
```

### Test

```shell
yarn run test
```

### Deployment

You can move only `dist/index.bundle.js` to anywhere to deploy

```shell
yarn run build
node dist/index.bundle.js
```
