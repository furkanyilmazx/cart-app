import COUPON_TYPE from './CouponTypes';
import { isNumber, isValidCouponType } from '@TrendyolCartApp/helper';

class Coupon {
  constructor(minAmount, value, couponType) {
    isNumber(value);
    isNumber(minAmount);
    isValidCouponType(couponType);

    this.value = value;
    this.minAmount = minAmount;
    this.couponType = couponType;
  }

  static isCouponInstance(coupon) {
    if (coupon && !(coupon instanceof Coupon))
      throw Error(
        `coupon must be instance of coupon class in applyCoupon function`
      );
    return true;
  }

  static isAvailableToApplyCoupon(totalAmount) {
    return totalAmount > 0;
  }

  calculateAppliedCouponPrice(cartTotal) {
    isNumber(cartTotal);

    if (cartTotal < this.minAmount) {
      return cartTotal;
    } else {
      return this._calculatePriceAccordingToCouponType(cartTotal);
    }
  }

  _calculatePriceAccordingToCouponType(price) {
    let calculatedPrice = 0;
    switch (this.couponType) {
      case COUPON_TYPE.AMOUNT:
        calculatedPrice = price - this.value;
        break;
      case COUPON_TYPE.RATE:
        calculatedPrice = price - (price * this.value) / 100;
        break;

      default:
        throw new Error('Invalid coupon type');
    }

    return Coupon.isAvailableToApplyCoupon(calculatedPrice)
      ? calculatedPrice
      : price;
  }
}

export default Coupon;
