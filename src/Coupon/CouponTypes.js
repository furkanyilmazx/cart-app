const COUPON_TYPE = {
  RATE: 'RATE',
  AMOUNT: 'AMOUNT',
};

export default Object.freeze(COUPON_TYPE);
