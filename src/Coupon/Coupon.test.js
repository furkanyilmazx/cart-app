import Coupon from './Coupon';
import { COUPON_TYPE } from '.';

describe('Coupon class', () => {
  it('isCouponInstance throw exception', () => {
    expect(() => Coupon.isCouponInstance({})).toThrow(Error);
  });

  it('isCouponInstance working properly', () => {
    const coupon = new Coupon(1000, 5, COUPON_TYPE.RATE);
    expect(Coupon.isCouponInstance(coupon)).toBe(true);
  });

  it('Coupon constructor working properly with wrong COUPON_TYPE', () => {
    expect(() => {
      const coupon = new Coupon(1000, 5, 'COUPON_TYPE.RATE');
    }).toThrow(Error);
  });

  it('Coupon isAvailableToApplyCoupon to be false', () => {
    expect(Coupon.isAvailableToApplyCoupon(0)).toBe(false);
  });

  it('Coupon isAvailableToApplyCoupon to be true', () => {
    expect(Coupon.isAvailableToApplyCoupon(100)).toBe(true);
  });

  it('Coupon calculateAppliedCouponPrice working properly COUPON_TYPE.RATE', () => {
    const coupon = new Coupon(1000, 5, COUPON_TYPE.RATE);
    expect(coupon.calculateAppliedCouponPrice(2000)).toBe(
      2000 - (2000 * 5) / 100
    );
  });

  it('Coupon calculateAppliedCouponPrice working properly COUPON_TYPE.AMOUNT', () => {
    const coupon = new Coupon(1000, 5, COUPON_TYPE.AMOUNT);
    expect(coupon.calculateAppliedCouponPrice(2000)).toBe(2000 - 5);
    expect(coupon.calculateAppliedCouponPrice(999)).toBe(999);
  });
  
});
