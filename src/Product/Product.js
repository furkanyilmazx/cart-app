import Category from '@TrendyolCartApp/Category';
import { isNumber } from '@TrendyolCartApp/helper';

class Product {
  constructor(title, price, category) {
    Category.isCategoryInstance(category);
    isNumber(price);

    this.title = title;
    this.price = price;
    this.category = category;
  }

  static isProductInstance(product) {
    if (product && !(product instanceof Product))
      throw new Error(
        'Invalid product type, product must be instance of Ppodcut class'
      );

    return true;
  }

  calculatePoductPriceWithQuantity(quantity) {
    if (isNaN(quantity)) throw new Error('quantity must be number');

    return this.price * quantity;
  }
}

export default Product;
