import Product from './Product';

describe('Product class', () => {
  it('isProductInstance throw exception', () => {
    expect(() => Product.isProductInstance({})).toThrow(Error);
  });
  
  it('isProductInstance working properly', () => {
    const product = new Product('dsadsa', 12);
    expect(() => Product.isProductInstance(product)).toBeTruthy();
  });

  it('calculatePoductPriceWithQuantity throw exception', () => {
    const product = new Product('dsadsa', 12);
    expect(() => product.calculatePoductPriceWithQuantity('dsadsa')).toThrow(
      Error
    );
  });

  it('calculatePoductPriceWithQuantity working properly', () => {
    const product = new Product('dsadsa', 12);
    expect(product.calculatePoductPriceWithQuantity(15)).toBe(180);
  });
});
