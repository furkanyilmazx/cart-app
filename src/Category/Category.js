class Category {
  constructor(title, subCategory) {
    Category.isCategoryInstance(subCategory);

    this.title = title;
    this.subCategory = subCategory;
  }

  static isCategoryInstance(category) {
    if (category && !(category instanceof Category))
      throw new Error(
        'Invalid category type, category must be instance of Ppodcut class'
      );
    return true;
  }

  isSameCategory(givenCategory) {
    return this.title === givenCategory.title;
  }
}

export default Category;
