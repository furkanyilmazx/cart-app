import Category from './Category';

describe('Category class', () => {
  it('isCategoryInstance throw exception', () => {
    expect(() => Category.isCategoryInstance({})).toThrow(Error);
  });

  it('Category constructor throw exception wrong type sub category', () => {
    expect(() => {
      const category = new Category('dsadsa', 12);
    }).toThrow(Error);
  });

  it('Category constructor working properly', () => {
    const subCategory = new Category('dsadsa');
    const category = new Category('dsadsa', subCategory);
    expect(Category.isCategoryInstance(category)).toBe(true);
  });

  it('Category isSameCategory working properly', () => {
    const category1 = new Category('dsadsa');
    const category2 = new Category('dsadsa');
    expect(category1.isSameCategory(category2)).toBe(true);
  });
  
  it('Category isSameCategory throw exception', () => {
    const category1 = new Category('aaaaaa');
    const category2 = new Category('dsadsa');
    expect(category1.isSameCategory(category2)).toBe(false);
  });

});
