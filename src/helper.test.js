import {
  sortByTitle,
  isNumber,
  isValidCampaignType,
  isValidCouponType,
  isValidQuantity,
} from '@TrendyolCartApp/helper';

import { CAMPAIGN_TYPE } from '@TrendyolCartApp/Campaign';
import { COUPON_TYPE } from '@TrendyolCartApp/Coupon';

describe('Helper tests', () => {
  it('sortByTitle working properly', () => {
    const givenArray = [
      {
        title: 'Dead Space',
      },
      {
        title: 'God of War 4',
      },
      {
        title: 'Zelda',
      },
      {
        title: 'Dead Space',
      },
      {
        title: 'Red Dead Redemption',
      },
    ];

    const expectedArray = [
      {
        title: 'Dead Space',
      },
      {
        title: 'Dead Space',
      },
      {
        title: 'God of War 4',
      },
      {
        title: 'Red Dead Redemption',
      },
      {
        title: 'Zelda',
      },
    ];

    // Sorting correct with helper
    expect(givenArray.sort((a, b) => sortByTitle(a, b))).toEqual(expectedArray);
  });

  it('isNumber working properly', () => {
    // Sorting correct with helper
    expect(isNumber(5)).toBe(true);
    expect(() => isNumber('dsadsa')).toThrow(Error);
  });

  it('isValidCampaignType working properly', () => {
    expect(isValidCampaignType(CAMPAIGN_TYPE.RATE)).toBe(true);
    expect(() => isValidCampaignType('ddddd')).toThrow(Error);
  });

  it('isValidCouponType working properly', () => {
    expect(isValidCouponType(COUPON_TYPE.RATE)).toBe(true);
    expect(() => isValidCouponType('ffffff')).toThrow(Error);
  });

  it('isValidQuantity working properly', () => {
    expect(isValidQuantity(15)).toBe(true);
    expect(() => isValidQuantity('ffffff')).toThrow(Error);
    expect(() => isValidQuantity(0)).toThrow(Error);
    expect(() => isValidQuantity(-1)).toThrow(Error);
  });
});
