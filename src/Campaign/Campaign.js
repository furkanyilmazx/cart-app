import Category from '@TrendyolCartApp/Category';
import CAMPAIGN_TYPE from './CampaignTypes';
import Product from '@TrendyolCartApp/Product';
import { isNumber, isValidCampaignType } from '@TrendyolCartApp/helper';

class Campaign {
  constructor(category, value, moreThanItems, campaignType) {
    Category.isCategoryInstance(category);
    isNumber(value);
    isNumber(moreThanItems);
    isValidCampaignType(campaignType);

    this.category = category;
    this.value = value;
    this.moreThanItems = moreThanItems;
    this.campaignType = campaignType;
  }

  static isCampaignInstance(campaign) {
    if (campaign && !(campaign instanceof Campaign))
      throw new Error(
        'Invalid campaign type, campaign must be instance of Campaign class'
      );
    return true;
  }

  calculateProductDiscountedPrice(product, quantity) {
    Product.isProductInstance(product);
    isNumber(quantity);

    if (this.isCampaignAvailable(product.category, quantity)) {
      return this._calculatePriceAccordingToDiscountType(
        product.price,
        quantity
      );
    } else {
      return product.price * quantity;
    }
  }

  _calculatePriceAccordingToDiscountType(price, quantity) {
    let amount = price * quantity;
    if (this.campaignType === CAMPAIGN_TYPE.AMOUNT) {
      return amount - this.value;
    } else if (this.campaignType === CAMPAIGN_TYPE.RATE) {
      return amount - (amount * this.value) / 100;
    } else {
      throw new Error('Invalid campaign type');
    }
  }

  isCampaignAvailable(category, quantity) {
    if (quantity < this.moreThanItems)
      throw new Error(
        `Doesnt calculated price because of this campaign available only more than ${this.moreThanItems}`
      );

    return this.category.isSameCategory(category);
  }
}

export default Campaign;
