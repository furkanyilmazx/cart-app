import Campaign from './Campaign';
import CAMPAIGN_TYPE from './CampaignTypes';
import Category from '@TrendyolCartApp/Category';
import Product from '@TrendyolCartApp/Product';

describe('Campaign class', () => {
  it('isCampaignInstance throw exception', () => {
    expect(() => Campaign.isCampaignInstance({})).toThrow(Error);
  });

  it('isCampaignInstance working properly', () => {
    const category = new Category('OEM');
    const campign = new Campaign(category, 10, 5, CAMPAIGN_TYPE.RATE);
    expect(Campaign.isCampaignInstance(campign)).toBeTruthy();
  });

  it('Campaign constructor throw exception with wrong campaign type', () => {
    expect(() => {
      const category = new Category('OEM');
      return new Campaign(category, 10, 5, 'CAMPAIGN_TYPE.RATE');
    }).toThrow(Error);
  });

  it('available campaign and calculate discount properly', () => {
    const category = new Category('OEM');
    const product = new Product('Omen600', 293, category);
    const campaignRated = new Campaign(category, 10, 5, CAMPAIGN_TYPE.RATE);
    const campaignAmounted = new Campaign(
      category,
      10,
      5,
      CAMPAIGN_TYPE.AMOUNT
    );

    expect(campaignAmounted.calculateProductDiscountedPrice(product, 5)).toBe(
      293 * 5 - 10
    );
    expect(campaignRated.calculateProductDiscountedPrice(product, 5)).toBe(
      293 * 5 - (293 * 5 * 10) / 100
    );
    expect(() => {
      const campaignUnknown = new Campaign(
        category,
        10,
        5,
        'UNKNOWN CAMPIGN TYPE'
      );
      campaignUnknown.calculateProductDiscountedPrice(product, 5);
    }).toThrow(Error);
  });

  it('Not available campaign', () => {
    const givenQuantity = 1;
    const expectedQuantity = 5;
    const category = new Category('OEM');
    const product = new Product('Omen600', 293, category);
    const campaign = new Campaign(
      category,
      10,
      expectedQuantity,
      CAMPAIGN_TYPE.RATE
    );
    expect(() =>
      campaign.calculateProductDiscountedPrice(product, givenQuantity)
    ).toThrow(Error);
  });

  it('is campaign available different category', () => {
    const expectedQuantity = 5;
    const oemCtegory = new Category('OEM');
    const menCategory = new Category('MEN');
    const campaign = new Campaign(
      menCategory,
      10,
      expectedQuantity,
      CAMPAIGN_TYPE.RATE
    );
    expect(campaign.isCampaignAvailable(oemCtegory, expectedQuantity)).toBe(
      false
    );
  });
});
