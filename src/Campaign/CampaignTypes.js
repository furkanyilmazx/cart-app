const CAMPAIGN_TYPE = {
  RATE: 'RATE',
  AMOUNT: 'AMOUNT',
};

export default Object.freeze(CAMPAIGN_TYPE);
