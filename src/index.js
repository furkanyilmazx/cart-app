import Product from './Product';
import Category from './Category';
import Cart from './Cart';
import Campaign from './Campaign';
import CampaignTypes from './Campaign/CampaignTypes';
import Coupon, { COUPON_TYPE } from './Coupon';
import DeliveryCostCalculator from './DeliveryCostCalculator';

//create Categories
const oemCategory = new Category('OEM');
const peripCategory = new Category('aPeripheral');

//create Products
const graphixCardProduct = new Product('gtx2070ti', 1500, oemCategory);
const graphixCardProduct2 = new Product('gtx2070tisd', 2500, oemCategory);
const keyboardProduct = new Product('omen600', 293, peripCategory);
const mouseProduct = new Product('omen300', 78, peripCategory);

//create Cart
const cart = new Cart();
cart.addItem(graphixCardProduct, 2);
cart.addItem(graphixCardProduct2, 1);
cart.addItem(keyboardProduct, 1);
cart.addItem(mouseProduct, 2);

//create Campaigns
const campaign = new Campaign(oemCategory, 5, 1, CampaignTypes.RATE);
const campaignofPerip = new Campaign(peripCategory, 7, 1, CampaignTypes.RATE);

//create Coupon
const coupon = new Coupon(1000, 10, COUPON_TYPE.RATE);

//create DeliveryCostCalculator for calculate costs
const deliveryCost = new DeliveryCostCalculator(2, 2, 2.99);

cart.applyDiscounts(campaign, campaignofPerip);
cart.applyCoupon(coupon);
deliveryCost.calculateFor(cart);

// Print Results
cart.print();
