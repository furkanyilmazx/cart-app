import Campaign from '@TrendyolCartApp/Campaign';
import Category from '@TrendyolCartApp/Category';
import Coupon from '@TrendyolCartApp/Coupon';
import Product from '@TrendyolCartApp/Product';

import { sortByTitle, isValidQuantity } from '@TrendyolCartApp/helper';

class Cart {
  constructor() {
    this.items = {};
    this.totalAmount = 0;
    this.totalCost = 0;
    this.couponDiscount = 0;
    this.campaignDiscount = 0;
  }

  static isCartInstance(cart) {
    if (cart && !(cart instanceof Cart))
      throw new Error('cart must be instance of Cart class');
  }

  set setTotalCost(totalCostz) {
    this.totalCost = totalCostz;
  }

  /* istanbul ignore next */
  get getTotalCost() {
    return this.totalCost;
  }

  set setTotalAmount(totalAmount) {
    this.totalAmount = totalAmount;
  }

  /* istanbul ignore next */
  get getTotalAmount() {
    return this.totalAmount;
  }

  set setCouponDiscont(couponDiscount) {
    this.couponDiscount = couponDiscount;
  }

  /* istanbul ignore next */
  get getCouponDiscount() {
    return this.couponDiscount;
  }

  set setCampaignDiscont(campaignDiscount) {
    this.campaignDiscount = campaignDiscount;
  }

  /* istanbul ignore next */
  get getCampaignDiscont() {
    return this.campaignDiscount;
  }

  getItems() {
    return Object.keys(this.items).map((item) => this.items[item]);
  }

  addItem(item, quantity) {
    Product.isProductInstance(item);
    isValidQuantity(quantity);

    // Add new item to cart or update quantity of exist item
    this.items[item.title] = {
      product: item,
      quantity: this.items[item.title]
        ? quantity + this.items[item.title].quantity
        : quantity,
    };

    // Reset total amount when add new product
    this.setTotalAmount = this.getTotalAmountOfCartWithoutDiscounts();
  }

  applyDiscounts(...campaigns) {
    //calculate discounted prices of whole campaigns
    const discountedAmounts = campaigns.map((campaign) => {
      Campaign.isCampaignInstance(campaign);

      let minAmountOfCart = 0;

      this.getItems().forEach((item) => {
        const { product, quantity } = item;
        let totalProductPriceWithoutDiscount = product.calculatePoductPriceWithQuantity(
          quantity
        );
        const productDiscountedPrice = campaign.calculateProductDiscountedPrice(
          product,
          quantity
        );
        if (productDiscountedPrice !== totalProductPriceWithoutDiscount) {
          item.discountedPrice = productDiscountedPrice;
        }
        minAmountOfCart = minAmountOfCart + productDiscountedPrice;
      });

      return minAmountOfCart;
    });

    // Choose Min value of whole discounted prices
    let totalAmountAfterCampaigns = Math.min(...discountedAmounts);

    let indexOfAppliedCampaign = discountedAmounts.indexOf(
      totalAmountAfterCampaigns
    );
    let appliedCampaign = campaigns[indexOfAppliedCampaign];

    this.getProductsExcludeWithCategory(appliedCampaign.category).forEach(
      (item) => {
        // reset discounted price
        item.discountedPrice = item.product.calculatePoductPriceWithQuantity(
          item.quantity
        );
      }
    );

    this.setCampaignDiscont = this.getTotalAmount - totalAmountAfterCampaigns;
    this.setTotalAmount = totalAmountAfterCampaigns;

    return this.getTotalAmount;
  }

  applyCoupon(coupon) {
    Coupon.isCouponInstance(coupon);

    if (!Coupon.isAvailableToApplyCoupon(this.getTotalAmount)) {
      throw new Error('Unsufficient amount of cart');
    }

    let totalAmountAfterCoupon = coupon.calculateAppliedCouponPrice(
      this.getTotalAmount
    );

    this.setCouponDiscont = this.getTotalAmount - totalAmountAfterCoupon;
    this.setTotalAmount = totalAmountAfterCoupon;

    return this.getTotalAmount;
  }

  getTotalAmountOfCartWithoutDiscounts() {
    let accInitalValue = 0;

    return this.getItems().reduce(
      (acc, { product, quantity }) =>
        acc + product.calculatePoductPriceWithQuantity(quantity),
      accInitalValue
    );
  }

  getNumberOfDifferentCategoriesInCart() {
    let productCategoriesTitles = this.getItems().map(
      ({ product }) => product.category && product.category.title
    );
    let uniqueProductCategoriesTitles = [...new Set(productCategoriesTitles)];

    return uniqueProductCategoriesTitles.length;
  }

  getNumberOfProductsInCart() {
    return this.getItems().length;
  }

  getNumberOfQuantityInCart() {
    let accInitalValue = 0;

    return this.getItems()
      .map((item) => item.quantity)
      .reduce((acc, quantity) => acc + quantity, accInitalValue);
  }

  getProductsExcludeWithCategory(category) {
    Category.isCategoryInstance(category);

    let productsAsCategory = this.getItems().filter(
      (item) =>
        item.product.category && item.product.category.title !== category.title
    );

    return productsAsCategory;
  }

  getDeliveryCost() {
    return this.getTotalCost;
  }

  getTotalAmountAfterDiscounts() {
    return this.getTotalAmount;
  }

  /* istanbul ignore next */
  print() {
    let cartStatics = this._generatePrintableCartObject();
    let cartTotals = this._generatePrintableTotalsOfCartObject();
    /* istanbul ignore next */
    console.table(cartStatics);
    /* istanbul ignore next */
    console.table(cartTotals);
  }

  /* istanbul ignore next */
  _generatePrintableCartObject() {
    return this.getItems()
      .sort((a, b) => sortByTitle(a.product.category, b.product.category))
      .map((item) => {
        let totalPriceWithQuantity = item.quantity * item.product.price;

        let couponDiscountPerProduct =
          this.getCouponDiscount *
          (item.discountedPrice /
            (this.getTotalAmount + this.getCouponDiscount));

        let campignDiscountPerProduct =
          totalPriceWithQuantity - item.discountedPrice;

        let totalDiscountCampaignAndCoupon =
          campignDiscountPerProduct + couponDiscountPerProduct;

        return {
          'Category Name': item.product.category.title,
          'Product Name': item.product.title,
          Quantity: item.quantity,
          'Unit Price': item.product.price,
          'Total Price': totalPriceWithQuantity,
          'Total Discount': totalDiscountCampaignAndCoupon,
        };
      });
  }

  /* istanbul ignore next */
  _generatePrintableTotalsOfCartObject() {
    return {
      'Total Amount After Discounts': this.getTotalAmount,
      'Total Amout of Delivery Costs': this.getTotalCost,
    };
  }
}

export default Cart;
