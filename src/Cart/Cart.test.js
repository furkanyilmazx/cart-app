import Product from '@TrendyolCartApp/Product';
import Category from '@TrendyolCartApp/Category';
import Campaign, { CAMPAIGN_TYPE } from '@TrendyolCartApp/Campaign';
import Coupon, { COUPON_TYPE } from '@TrendyolCartApp/Coupon';
import DeliveryCostCalculator from '@TrendyolCartApp/DeliveryCostCalculator';
import Cart from '@TrendyolCartApp/Cart';

describe('Cart class', () => {
  it('Cart working properly', () => {
    const oemCategory = new Category('OEM');
    const peripheralCategory = new Category('aPeripheral');

    const graphixCardProduct = new Product('gtx2070ti', 1500, oemCategory);
    const graphixCardProduct2 = new Product('gtx2070tisd', 2500, oemCategory);
    const keyboardProduct = new Product('omen600', 293, peripheralCategory);
    const mouseProduct = new Product('omen300', 78, peripheralCategory);

    const cart = new Cart();
    cart.addItem(graphixCardProduct, 2);
    cart.addItem(graphixCardProduct2, 1);
    cart.addItem(keyboardProduct, 1);
    cart.addItem(mouseProduct, 2);

    const campaign = new Campaign(oemCategory, 5, 1, CAMPAIGN_TYPE.RATE);
    const campaignofPeripheral = new Campaign(
      peripheralCategory,
      7,
      1,
      CAMPAIGN_TYPE.RATE
    );

    const coupon = new Coupon(1000, 10, COUPON_TYPE.RATE);

    // We have 2 discount campign one of them oem category %5,
    // the other peripheral category %7
    // but we should apply maximum discountable campign to cart
    // and Oem products have huge amount as discount
    // so (2500 + 3000) - ((2500 + 3000) * 5 /100) and sum with 293 + 2 * 78
    // Result  must be 5674
    expect(cart.applyDiscounts(campaign, campaignofPeripheral)).toBe(
      2500 + 3000 - ((2500 + 3000) * 5) / 100 + 293 + 2 * 78
    );

    // Total amount of Cart is 5674 for now
    // If we would apply coupon this amount to be 5106.6
    // Because coupon minimum amount greater than 1000 and our cart total amount is
    // 5674.
    // 5674 - (5674 * 10 / 100)
    expect(cart.applyCoupon(coupon)).toBe(5674 - (5674 * 10) / 100);

    // must be 5106.6
    expect(cart.getTotalAmountAfterDiscounts()).toBe(5106.6);

    // apply not available coupon
    cart.setTotalAmount = 0;
    expect(() => cart.applyCoupon(coupon)).toThrow(Error);

    expect(cart.getNumberOfQuantityInCart()).toBe(6);

    const deliveryCost = new DeliveryCostCalculator(2, 2, 2.99);
    deliveryCost.calculateFor(cart);
    expect(cart.getDeliveryCost()).toBe(14.99);
  });
});
