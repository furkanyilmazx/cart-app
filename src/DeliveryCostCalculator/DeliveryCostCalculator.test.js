import Product from '@TrendyolCartApp/Product';
import Category from '@TrendyolCartApp/Category';
import Campaign, { CAMPAIGN_TYPE } from '@TrendyolCartApp/Campaign';
import Coupon, { COUPON_TYPE } from '@TrendyolCartApp/Coupon';
import Cart from '@TrendyolCartApp/Cart';

import DeliveryCostCalculator from './DeliveryCostCalculator';

describe('DeliveryCostCalculator class', () => {
  it('DeliveryCostCalculator working properly', () => {
    const oemCategory = new Category('OEM');
    const peripheralCategory = new Category('aPeripheral');

    const graphixCardProduct = new Product('gtx2070ti', 1500, oemCategory);
    const graphixCardProduct2 = new Product('gtx2070tisd', 2500, oemCategory);
    const keyboardProduct = new Product('omen600', 293, peripheralCategory);
    const mouseProduct = new Product('omen300', 78, peripheralCategory);

    const cart = new Cart();
    cart.addItem(graphixCardProduct, 2);
    cart.addItem(graphixCardProduct2, 1);
    cart.addItem(keyboardProduct, 1);
    cart.addItem(mouseProduct, 2);

    const campaign = new Campaign(oemCategory, 5, 1, CAMPAIGN_TYPE.RATE);
    const campaignofPeripheral = new Campaign(
      peripheralCategory,
      7,
      1,
      CAMPAIGN_TYPE.RATE
    );

    const coupon = new Coupon(1000, 10, COUPON_TYPE.RATE);

    const deliveryCost = new DeliveryCostCalculator(2, 2, 2.99);

    cart.applyDiscounts(campaign, campaignofPeripheral);
    cart.applyCoupon(coupon);

    // We have 2 different category and we have 4 product in cart
    // (2 * 2 ) + (4 * 2) + 2.99
    expect(deliveryCost.calculateNumberOfProducts(cart)).toBe(4);
    expect(deliveryCost.calculateNumberOfDeliveries(cart)).toBe(2);
    expect(deliveryCost.calculateFor(cart)).toBe(2 * 2 + 4 * 2 + 2.99);

    //throw error test
    expect(() => {
      deliveryCost.calculateFor({});
    }).toThrow(Error);

    // Default value test
    const deliveryCost2 = new DeliveryCostCalculator(2, 2);
    expect(deliveryCost2.fixedCost).toBe(2.99);
  });
});
