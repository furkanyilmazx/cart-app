import Cart from '@TrendyolCartApp/Cart';

class DeliveryCostCalculator {
  constructor(costPerDelivery, costPerProduct, fixedCost = 2.99) {
    this.costPerDelivery = costPerDelivery;
    this.costPerProduct = costPerProduct;
    this.fixedCost = fixedCost;
  }

  calculateFor(cart) {
    try {
      let numberOfProducts = this.calculateNumberOfProducts(cart);
      let numberOfDeliveries = this.calculateNumberOfDeliveries(cart);
      let totalCost =
        this.costPerDelivery * numberOfDeliveries +
        this.costPerProduct * numberOfProducts +
        this.fixedCost;

      cart.setTotalCost = totalCost;
      return totalCost;
    } catch (error) {
      throw new Error(
        'DeliveryCostCalculator doesnt calculated because: ',
        error.message
      );
    }
  }

  calculateNumberOfProducts(cartInstance) {
    let cart = this._getCart(cartInstance);
    return cart.getNumberOfProductsInCart();
  }

  calculateNumberOfDeliveries(cartInstance) {
    let cart = this._getCart(cartInstance);
    return cart.getNumberOfDifferentCategoriesInCart();
  }

  _getCart(cart) {
    Cart.isCartInstance(cart);

    return cart;
  }
}

export default DeliveryCostCalculator;
