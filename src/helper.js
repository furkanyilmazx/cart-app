import CAMPAIGN_TYPE from '@TrendyolCartApp/Campaign/CampaignTypes';
import COUPON_TYPE from '@TrendyolCartApp/Coupon/CouponTypes';

export function sortByTitle(a, b) {
  if (a.title < b.title) {
    return -1;
  }
  if (a.title > b.title) {
    return 1;
  }
  return 0;
}

export function isNumber(number) {
  if (isNaN(number)) throw new Error('Given variable type must be number');
  return true;
}

export function isValidCampaignType(campaignType) {
  if (campaignType && !CAMPAIGN_TYPE[campaignType])
    throw new Error('campaignType must be in CAMPAIGN_TYPE');
  return true;
}

export function isValidCouponType(couponType) {
  if (couponType && !COUPON_TYPE[couponType])
    throw new Error('couponType must be in COUPON_TYPE');
  return true;
}

export function isValidQuantity(quantity) {
  if (isNaN(quantity) || quantity <= 0)
    throw new Error(
      'Invalid quantity type, quantity must be instance of number or greater than 0'
    );

  return true;
}
